import Vue from 'vue';
import Vuex from 'vuex';
import {marker} from '@/store/modules/marker/marker';
import {RootState} from "@/store/types";

Vue.use(Vuex);

export default new Vuex.Store<RootState>({

    state: {
        applicationName: 'map',
    },

    modules: {
        marker,
    },

})
