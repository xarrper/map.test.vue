import IMarker from "@/domain/entities/interfaces/IMarker";

interface MarkerState {
    marker: IMarker;
    markers: IMarker[];
}

export default MarkerState;
