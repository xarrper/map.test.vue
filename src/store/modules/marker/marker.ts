import {ActionTree, GetterTree, MutationTree} from "vuex";
import {RootState} from "@/store/types";
import MarkerState from "@/store/modules/marker/types";
import Marker from "@/domain/entities/Marker";
import IMarker from "@/domain/entities/interfaces/IMarker";

export const state: MarkerState = {
    markers: [new Marker('', [-1, -1])],
    marker: new Marker('', [-1, -1]),
};

export const getters: GetterTree<MarkerState, RootState> = {
    getLines: state => {
        let lines: Array<{ coords: [[number, number], [number, number]] }> = [];
        for (let i = 0; i < (state.markers.length - 1); i++) {
            lines.push({
                coords: [state.markers[i].coords, state.markers[i + 1].coords]
            });
        }
        return lines;
    },
};

export const actions: ActionTree<MarkerState, RootState> = {

    allMarkers({commit}) {
        let data = [
            new Marker('Marker-1', [55.75222, 37.61556]),
            new Marker('Marker-2', [55.721421398460826, 37.538999025878894]),
            new Marker('Marker-3', [55.746217156485365, 37.76627868896484]),
            new Marker('Marker-4', [55.70824221749287, 37.61556]),
        ];
        commit('setMarkers', data);
    },

    saveMarker({commit}) {
        commit('pushMarker');
    },

    updateMarker({commit}, playload: { index: number, coords: [number, number] }) {
        commit('setMarker', playload);
    },

    destroyMarker({commit}, playload: number) {
        commit('removeMarker', playload);
    }

};

export const mutations: MutationTree<MarkerState> = {

    setMarkers(state, payload: IMarker[]) {
        state.markers = payload;
    },

    setMarker(state, payload: { index: number, coords: [number, number] }) {
        state.markers[payload.index].coords = payload.coords;
    },

    pushMarker(state) {
        state.markers.push(state.marker);
        state.marker = new Marker('', [-1, -1]);
    },

    removeMarker(state, playload: number) {
        state.markers.splice(playload, 1);
    }

};

export const marker = {
    state,
    getters,
    actions,
    mutations,
};
