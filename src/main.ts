import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'

const settings = {
    apiKey: process.env.VUE_APP_API_KEY,
    lang: 'ru_RU',
    version: '2.1'
}

/**
 * Подключаемые пакеты
 * TODO: tsconfig "noImplicitAny": false
 */
import YmapPlugin from 'vue-yandex-maps';

Vue.use(YmapPlugin, settings);

import VModal from 'vue-js-modal'

Vue.use(VModal);

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
