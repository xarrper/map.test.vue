interface IMarker {
    id?: number;
    name: string;
    coords: [number, number];
}

export default IMarker;
