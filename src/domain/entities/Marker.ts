import IMarker from "@/domain/entities/interfaces/IMarker";

class Marker implements IMarker {

    public id?: number;
    public name: string;
    public coords: [number, number];

    constructor(name: string, coords: [number, number], id: number | undefined = undefined) {
        this.id = id;
        this.name = name;
        this.coords = coords;
    }

}

export default Marker;
